function stringContract() {
    var token_name = "TSK";
    var token_symbol = "TSK"
    var decimals = 0
    var total_supply = 21000000000000000
    return 'pragma solidity ^0.4.15; contract Token {function totalSupply() constant returns (uint256 supply) {}function balanceOf(address _owner) constant returns (uint256 balance) {}function transfer(address _to, uint256 _value) returns (bool success) {}function transferFrom(address _from, address _to, uint256 _value) returns (bool success) {}function approve(address _spender, uint256 _value) returns (bool success) {}function allowance(address _owner, address _spender) constant returns (uint256 remaining) {}event Transfer(address indexed _from, address indexed _to, uint256 _value);event Approval(address indexed _owner, address indexed _spender, uint256 _value);}contract StandardToken is Token {function transfer(address _to, uint256 _value) returns (bool success) {if (balances[msg.sender] >= _value && _value > 0) {balances[msg.sender] -= _value;balances[_to] += _value;Transfer(msg.sender, _to, _value);return true;} else { return false; }}function transferFrom(address _from, address _to, uint256 _value) returns (bool success) {if (balances[_from] >= _value && allowed[_from][msg.sender] >= _value && _value > 0) {balances[_to] += _value;balances[_from] -= _value;allowed[_from][msg.sender] -= _value;Transfer(_from, _to, _value);return true;} else { return false; }}function balanceOf(address _owner) constant returns (uint256 balance) {return balances[_owner];}function approve(address _spender, uint256 _value) returns (bool success) {allowed[msg.sender][_spender] = _value;Approval(msg.sender, _spender, _value);return true;}function allowance(address _owner, address _spender) constant returns (uint256 remaining) {return allowed[_owner][_spender];}mapping (address => uint256) balances;mapping (address => mapping (address => uint256)) allowed;uint256 public totalSupply;}contract ERC20Token is StandardToken {function () {revert();}string public name;uint8 public decimals;string public symbol;string public version = "H1.0";function ERC20Token() {balances[msg.sender] = '+ total_supply.toString() +';totalSupply = '+ parseInt(total_supply) +';name = "'+ token_name +'";decimals = '+parseInt(decimals)+';symbol = "'+ token_symbol +'";}function approveAndCall(address _spender, uint256 _value, bytes _extraData) returns (bool success) {allowed[msg.sender][_spender] = _value;Approval(msg.sender, _spender, _value);if(!_spender.call(bytes4(bytes32(sha3("receiveApproval(address,uint256,address,bytes)"))), msg.sender, _value, this, _extraData)) { revert(); }return true;}}' ;
}

$("#task-submit").click((e) => {
    e.preventDefault();
    var token_name = $("#token_name").val();
    var token_symbol = $("#token_symbol").val();
    var decimals = $("#decimals").val();
    var total_supply = $("#total_supply").val();
    var strContract = stringContract();
    makeContract(strContract);
})

function makeContract(strContract) {
    var output; var bytecode; var abi;
    BrowserSolc.loadVersion("soljson-v0.4.15+commit.bbb8e64f.js", function(compiler) {
      
      var output = compiler.compile(strContract, 1);
      console.log(output);
      var bytecode = output.contracts[':ERC20Token'].bytecode;
      var abi = JSON.parse(output.contracts[':ERC20Token'].interface);
      var contract = web3.eth.contract(abi);
      const contractInstance = contract.new({
        data: '0x' + bytecode,
        from: web3.eth.defaultAccount,
        gas: 90000*10
        }, (err, res) => {
                if (err) {
                    console.log(err);
                    return;
                }
            });
        });
  }
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var authSession = new Schema({
  address:  String
});

module.exports = mongoose.model('Auth', authSession);
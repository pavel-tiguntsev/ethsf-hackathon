var express = require('express');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

app.use(express.static('public'))
app.set('view engine', 'ejs');
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false })); 

var configDB = require('./config/database.js');
mongoose.connect(configDB.url);

var Auth = require('./models/auth')
var User = require('./models/auth')

app.get('/auth', function(req, res) {
    var eth_address = req.query.address;
    var secret = req.query.secret;
    var newAuth = new Auth({"address" : eth_address, "secret": secret});
    newAuth.save(() => {
        res.send("authorization begin...");
    })
});

app.get('/check_auth', function(req, res) {
    var eth_address = req.query.address;
    var secret = req.query.secret;
    Auth.findOne({
        address: eth_address
    }, (err, doc) => {
        if (doc) {
            res.send('/home?address=' + eth_address + '&secret=' + secret)
        } else {
            res.send("auth_failed")
        }
    })
});

app.get('/home', function(req, res) {
    User.findOne({
        address: req.query.address
        // secret: req.query.secret
    }, (err, user) => {
        if (user) {
            res.render('pages/home', {user: user});
        }
    });
});

app.get('/ico', function(req, res) {
    res.render('pages/ico');
});

app.get('/org', function(req, res) {
    res.render('pages/org');
});

app.get('/bloomers', function(req, res) {
    res.render('pages/bloomers');
});

app.get('/tasks', function(req, res) {
    res.render('pages/tasks');
});

app.get('/loans', function(req, res) {
    res.render('pages/loans');
});

app.get('/blancers', function(req, res) {
    res.render('pages/blancers');
});



app.listen(3000);
console.log('3000 is the magic port');

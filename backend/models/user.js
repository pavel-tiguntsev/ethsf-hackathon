var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var user = new Schema({
  address:  String,
  name: String,
  userId:   String,
  secret: String,
  date: { type: Date, default: Date.now }
});

module.exports = mongoose.model('User', user);